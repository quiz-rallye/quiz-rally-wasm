﻿using Microsoft.EntityFrameworkCore;
using Route = QuizRally.Database.Entities.References.Route;

namespace QuizRally.Database.Entities;

[Index(nameof(RallyId), nameof(RouteId), IsUnique = true)]
public class Team
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public int RallyId { get; set; }
    public Rally Rally { get; set; } = null!;

    public int RouteId { get; set; }
    public Route Route { get; set; } = null!;

    public virtual List<Trace> Traces { get; set; } = null!;
}
