using Microsoft.EntityFrameworkCore;
using QuizRally.Database.Entities.References;

namespace QuizRally.Database.Entities;

[Index(nameof(TeamId), nameof(PointOfInterestId), IsUnique = true)]
public class Trace
{
    public int Id { get; set; }

    public Team Team { get; set; } = null!;
    public int TeamId { get; set; }

    public PointOfInterest PointOfInterest { get; set; } = null!;
    public int PointOfInterestId { get; set; }

    public DateTime TraceDate { get; set; }
}
