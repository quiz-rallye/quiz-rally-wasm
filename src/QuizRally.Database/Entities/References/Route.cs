﻿namespace QuizRally.Database.Entities.References;

public class Route
{
    public int Id { get; set; }

    public string Name { get; set; } = "";
    public string Description { get; set; } = "";
    public List<RoutePath> RoutePathsList { get; set; } = new();

    public int EstimatedTimeMinutes { get; set; }
    public string StartInformation { get; set; } = null!;
    public string EndInformation { get; set; } = null!;
}

public static class RouteExtensions
{
    public static RoutePath GetStartPath(this Route route)
    {
        // TODO test this
        return route.RoutePathsList.First(rp => !route.RoutePathsList.Exists(rpEnd => rpEnd.EndPoi.Id == rp.StartPoi.Id));
    }
}
