namespace QuizRally.Database.Entities.References;

public class RoutePath
{
    public int Id { get; set; }
    
    public Route Route { get; set; } = null!;
    public int RouteId { get; set; }
    
    public string Description { get; set; } = "";
    public PointOfInterest StartPoi { get; set; } = null!;
    public PointOfInterest EndPoi { get; set; } = null!;
}
