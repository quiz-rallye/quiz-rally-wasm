namespace QuizRally.Database.Entities;

public class Rally
{
    public int Id { get; set; }

    public string JoinCode { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string ScenarioDescription { get; set; } = string.Empty;
}
