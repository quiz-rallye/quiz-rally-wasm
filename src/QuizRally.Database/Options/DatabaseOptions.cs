namespace QuizRally.Database.Options;

public class DatabaseOptions
{
    public const string Option = "Database";

    public string Host { get; set; } = null!;
    public int Port { get; set; }
    public string Username { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string Database { get; set; } = null!;

    public string GetConnectionString()
    {
        return $"User Id={Username};Password={Password};Server={Host};Port={Port};Database={Database};";
    }
}