using Microsoft.EntityFrameworkCore;
using QuizRally.Database.Context;
using QuizRally.Database.Options;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddDbContext<QuizRallyContext>(
    options =>
    {
        var dbConfig = builder.Configuration.GetRequiredSection(DatabaseOptions.Option).Get<DatabaseOptions>()!;
        options.UseNpgsql(dbConfig.GetConnectionString());
    });

var app = builder.Build();

app.Run();


