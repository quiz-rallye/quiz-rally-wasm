using Microsoft.EntityFrameworkCore;
using QuizRally.Database.Entities;
using QuizRally.Database.Entities.References;
using Route = QuizRally.Database.Entities.References.Route;

namespace QuizRally.Database.Context;

public class QuizRallyContext : DbContext
{
    public DbSet<Route> Routes { get; set; } = null!;
    public DbSet<RoutePath> RoutePaths { get; set; } = null!;
    public DbSet<PointOfInterest> PointOfInterests { get; set; } = null!;
    public DbSet<Rally> Rallies { get; set; } = null!;
    public DbSet<Team> Teams { get; set; } = null!;
    public DbSet<Trace> Traces { get; set; } = null!;

    public QuizRallyContext(DbContextOptions<QuizRallyContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
    }
}
