using FisSst.BlazorMaps.DependencyInjection;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor.Services;
using QuizRally.Web;
using QuizRally.Web.Services;
using Supabase;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddBlazorLeafletMaps();
builder.Services.AddGeolocationServices();


var url = "https://bqmomdefktzrtuuoiwln.supabase.co";
var key =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImJxbW9tZGVma3R6cnR1dW9pd2xuIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzUwOTkyNzYsImV4cCI6MTk5MDY3NTI3Nn0.NkWJp2Y9citS5dW-vrqEeTl_YOTb76AKFlueFF8IDGg";

builder.Services.AddScoped<Client>(provider => new Client(url, key));
builder.Services.AddMudServices();

builder.Services.AddScoped<IPoiService, PoiService>();
builder.Services.AddScoped<IRoutePathService, RoutePathService>();
builder.Services.AddScoped<IRouteService, RouteService>();
builder.Services.AddScoped<ITeamService, TeamService>();
builder.Services.AddScoped<IRallyService, RallyService>();

await builder.Build().RunAsync();
