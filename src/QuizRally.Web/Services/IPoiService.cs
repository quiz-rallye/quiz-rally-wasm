﻿using QuizRally.Web.Dtos.References;

namespace QuizRally.Web.Services;

public interface IPoiService
{
    Task<PointOfInterest?> GetPoiByIdAsync(int id);
    Task<List<PointOfInterest>> GetAllPoisAsync();
}
