using QuizRally.Web.Dtos;
using Supabase;

namespace QuizRally.Web.Services;

public class TeamService : ITeamService
{
    private Client _supabaseClient;

    public TeamService(Client supabaseClient)
    {
        _supabaseClient = supabaseClient;
    }

    public async Task<List<Team>> GetAllTeamsAsync()
    {
        var response = await _supabaseClient
            .From<Team>()
            .Select("*")
            .Get();
        return response.Models;
    }

    public async Task<Team?> GetTeamByIdAsync(int teamId)
    {
        var response = await _supabaseClient
            .From<Team>()
            .Select("*, Route:RouteId(*), Traces(*, PointOfInterest:PointOfInterestId(*))")
            .Where(t => t.Id == teamId)
            .Get();
        return response.Models.FirstOrDefault();
    }

    public async Task<List<Team>> GetFreeTeamsAsync(int rallyId)
    {
        var response = await _supabaseClient
            .From<Team>()
            .Select("*")
            .Where(team => team.RallyId == rallyId)
            .Where(team => team.Name == null)
            .Get();
        return response.Models;
    }

    public async Task<List<Team>> GetOccupiedTeamsAsync(int rallyId)
    {
        var response = await _supabaseClient
            .From<Team>()
            .Select("*")
            .Where(team => team.RallyId == rallyId)
            .Where(team => team.Name != null)
            .Get();
        return response.Models;
    }

    public async Task JoinTeam(int teamId, string teamName)
    {
        await _supabaseClient
            .From<Team>()
            .Where(team => team.Id == teamId)
            .Where(team => team.Name == null)
            .Set(team => team.Name, teamName)
            .Update();
    }
}
