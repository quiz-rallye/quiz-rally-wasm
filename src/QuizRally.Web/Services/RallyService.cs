using QuizRally.Web.Dtos;
using Supabase;

namespace QuizRally.Web.Services;

public class RallyService: IRallyService
{
    private Client _supabaseClient;

    public RallyService(Client supabaseClient)
    {
        _supabaseClient = supabaseClient;
    }

    public async Task<List<Rally>> GetAllRalliesAsync()
    {
        var response = await _supabaseClient
            .From<Rally>()
            .Select("*, Teams(*)")
            .Get();
        return response.Models;
    }

    public async Task<Rally?> GetRallyByIdAsync(int id)
    {
        var response = await _supabaseClient
            .From<Rally>()
            .Select("*, Teams(*, Route:RouteId(*, RoutePaths(*, StartPoi:StartPoiId(*), EndPoi:EndPoiId(*))), Traces(*))")
            .Where(rally => rally.Id == id)
            .Get();
        return response.Models.FirstOrDefault();
    }

    public async Task<Rally?> GetRallyByJoinCodeAsync(string joinCode)
    {
        var response = await _supabaseClient
            .From<Rally>()
            .Select("*, Teams(*, Route:RouteId(*, RoutePaths(*, StartPoi:StartPoiId(*), EndPoi:EndPoiId(*))), Traces(*))")
            .Where(rally => rally.JoinCode == joinCode)
            .Get();
        return response.Models.FirstOrDefault();
    }
}
