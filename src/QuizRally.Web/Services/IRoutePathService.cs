﻿using QuizRally.Web.Dtos.References;

namespace QuizRally.Web.Services;

public interface IRoutePathService
{
    Task<List<RoutePath>> GetAllRoutePathsAsync();
}
