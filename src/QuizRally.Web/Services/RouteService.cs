using QuizRally.Web.Dtos.References;
using Supabase;

namespace QuizRally.Web.Services;

public class RouteService : IRouteService
{
    private Client _supabaseClient;

    public RouteService(Client supabaseClient)
    {
        _supabaseClient = supabaseClient;
    }

    public async Task<List<Route>> GetAllRoutesAsync()
    {
        var response = await _supabaseClient
            .From<Route>()
            .Select("*, RoutePaths(*, StartPoi:StartPoiId(*), EndPoi:EndPoiId(*))")
            .Get();

        return response.Models;
    }
}
