using QuizRally.Web.Dtos.References;

namespace QuizRally.Web.Services;

public interface IRouteService
{
    Task<List<Route>> GetAllRoutesAsync();
}
