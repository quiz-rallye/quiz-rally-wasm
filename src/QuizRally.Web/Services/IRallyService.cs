using QuizRally.Web.Dtos;

namespace QuizRally.Web.Services;

public interface IRallyService
{
    Task<List<Rally>> GetAllRalliesAsync();
    Task<Rally?> GetRallyByIdAsync(int id);
    Task<Rally?> GetRallyByJoinCodeAsync(string joinCode);
}
