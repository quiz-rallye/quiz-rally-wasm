﻿using QuizRally.Web.Dtos.References;
using Supabase;

namespace QuizRally.Web.Services;

public class RoutePathService : IRoutePathService
{
    private Client _supabaseClient;

    public RoutePathService(Client supabaseClient)
    {
        _supabaseClient = supabaseClient;
    }

    public async Task<List<RoutePath>> GetAllRoutePathsAsync()
    {
        var response = await _supabaseClient
            .From<RoutePath>()
            .Select("*, StartPoi:StartPoiId(*), EndPoi:EndPoiId(*)")
            .Get();
        return response.Models;
    }
}
