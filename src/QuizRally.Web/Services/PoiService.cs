﻿using QuizRally.Web.Dtos.References;
using Supabase;

namespace QuizRally.Web.Services;

public class PoiService : IPoiService
{
    private Client _supabaseClient;

    public PoiService(Client supabaseClient)
    {
        _supabaseClient = supabaseClient;
    }

    public async Task<PointOfInterest?> GetPoiByIdAsync(int id)
    {
        var response = await _supabaseClient
            .From<PointOfInterest>()
            .Select("*")
            .Where(poi => poi.Id == id)
            .Get();

        return response.Models.FirstOrDefault();
    }

    public async Task<List<PointOfInterest>> GetAllPoisAsync()
    {
        var response = await _supabaseClient
            .From<PointOfInterest>()
            .Select("*")
            .Get();
        return response.Models;
    }
}
