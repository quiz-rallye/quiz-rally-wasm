using QuizRally.Web.Dtos;

namespace QuizRally.Web.Services;

public interface ITeamService
{
    Task<List<Team>> GetAllTeamsAsync();
    Task<Team?> GetTeamByIdAsync(int teamId);
    Task<List<Team>> GetFreeTeamsAsync(int rallyId);
    Task<List<Team>> GetOccupiedTeamsAsync(int rallyId);
    Task JoinTeam(int teamId, string teamName);

}
