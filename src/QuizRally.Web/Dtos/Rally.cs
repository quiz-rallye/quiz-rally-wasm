using Postgrest.Attributes;
using Postgrest.Models;

namespace QuizRally.Web.Dtos;

[Table("Rallies")]
public class Rally : BaseModel
{
    [PrimaryKey]
    public int Id { get; set; }

    [Column]
    public string JoinCode { get; set; } = null!;

    [Column]
    public string Name { get; set; } = null!;

    [Column]
    public string ScenarioDescription { get; set; } = string.Empty;

    [Column]
    public List<Team> Teams { get; set; } = null!;
}

public static class RallyExtensions
{
    public static int GetFreeTeamsCount(this Rally rally)
    {
        return rally.Teams.Count(team => team.Name == null);
    }

    public static Team? GetNextFreeTeam(this Rally rally)
    {
        return rally.Teams.FirstOrDefault(team => team.Name == null);
    }
}
