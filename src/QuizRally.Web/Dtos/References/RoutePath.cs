using Postgrest.Attributes;
using Postgrest.Models;

namespace QuizRally.Web.Dtos.References;

[Table("RoutePaths")]
public class RoutePath : BaseModel
{
    [PrimaryKey]
    public int Id { get; set; }

    [Column]
    public int RouteId { get; set; }

    [Column]
    public string Description { get; set; }

    [Column]
    public PointOfInterest StartPoi { get; set; }

    [Column]
    public PointOfInterest EndPoi { get; set; }
}
