using Postgrest.Attributes;
using Postgrest.Models;

namespace QuizRally.Web.Dtos.References;

[Table("Routes")]
public class Route : BaseModel
{
    [PrimaryKey]
    public int Id { get; set; }

    [Column]
    public string Name { get; set; } = string.Empty;

    [Column]
    public string Description { get; set; } = string.Empty;

    [Column]
    public int EstimatedTimeMinutes { get; set; }

    [Column]
    public string StartInformation { get; set; } = string.Empty;

    [Column]
    public string EndInformation { get; set; } = "";

    [Column]
    public List<RoutePath> RoutePaths { get; set; } = new();
}
