using Postgrest.Attributes;
using Postgrest.Models;

namespace QuizRally.Web.Dtos.References;

[Table("PointOfInterests")]
public class PointOfInterest : BaseModel
{
    [PrimaryKey]
    public int Id { get; set; }

    [Column]
    public string Name { get; set; } = string.Empty;

    [Column]
    public double Longitude { get; set; }

    [Column]
    public double Latitude { get; set; }

    [Column]
    public string Description { get; set; } = string.Empty;
}
