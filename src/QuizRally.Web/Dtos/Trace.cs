using Postgrest.Attributes;
using Postgrest.Models;
using QuizRally.Web.Dtos.References;

namespace QuizRally.Web.Dtos;

[Table("Traces")]
public class Trace : BaseModel
{
    [PrimaryKey]
    public int Id { get; set; }

    [Column]
    public int TeamId { get; set; }

    [Column]
    public int PointOfInterestId { get; set; }

    [Column]
    public PointOfInterest PointOfInterest { get; set; } = null!;

    [Column]
    public DateTime TraceDate { get; set; }
}
