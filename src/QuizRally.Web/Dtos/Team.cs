﻿using Postgrest.Attributes;
using Postgrest.Models;
using QuizRally.Web.Dtos.References;

namespace QuizRally.Web.Dtos;

[Table("Teams")]
public class Team : BaseModel
{
    [PrimaryKey]
    public int Id { get; set; }

    [Column]
    public string? Name { get; set; }

    [Column]
    public int RallyId { get; set; }

    [Column]
    public int RouteId { get; set; }

    [Column]
    public Route Route { get; set; } = null!;

    [Column]
    public virtual List<Trace> Traces { get; set; } = null!;
}
